package eu.olol.ork.bmi;

import java.text.NumberFormat;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends Activity {

	EditText weight;
	EditText length;
	Button compute;
	TextView result;
	TextView comment;
	NumberFormat nf;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		weight = (EditText) findViewById(R.id.weight);
		length = (EditText) findViewById(R.id.length);
		compute = (Button) findViewById(R.id.compute);
		result = (TextView) findViewById(R.id.result);
		comment = (TextView) findViewById(R.id.comment);
		compute.setOnClickListener(computeListener);
		nf = NumberFormat.getInstance();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

	private OnClickListener computeListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			String w = weight.getText().toString();
			String l = length.getText().toString();
			if (w.matches("") || l.matches("")) return;
			Float bmi = 10000 * Float.valueOf(w) / (Float.valueOf(l) * Float.valueOf(l));
			if (bmi > 400.0 || bmi < 2) return;
			result.setText(nf.format(bmi));

			if (bmi < 15.0) {
				comment.setText(R.string.very_severely_underweight);
			} else if (bmi >= 15.0 && bmi < 16.0) {
				comment.setText(R.string.severely_underweight);
			} else if (bmi >= 16.0 && bmi < 18.5) {
				comment.setText(R.string.underweight);
			} else if (bmi >= 18.5 && bmi < 25.0) {
				comment.setText(R.string.normal_weight);
			} else if (bmi >= 25.0 && bmi < 30.0) {
				comment.setText(R.string.overweight);
			} else if (bmi >= 30.0 && bmi < 35.0) {
				comment.setText(R.string.obese_class_1);
			} else if (bmi >= 35.0 && bmi < 40.0) {
				comment.setText(R.string.obese_class_2);
			} else {
				comment.setText(R.string.obese_class_3);
			}
		}
	};

	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
		case R.id.menu_reset:
			weight.getText().clear();
			length.getText().clear();
			result.setText(R.string.bmi);
			comment.setText(null);
			weight.requestFocus();
			break;

		default:
			break;
		}

		return true;
	};
}
